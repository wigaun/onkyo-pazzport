# Onkyo iPazzPort #

Controlling Onkyo Zone 2 TuneIn stations with iPazzPort bluetooth 
on Jumper EZbox N4 mini PC running Debian Linux

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration

    python3 -m venv pyenv
    source pyenv/bin/activate
    python3 -m pip install --upgrade pip evdev onkyo-eiscp schedule

Add yourself into input /etc/group

See https://superuser.com/questions/699905/change-behavior-of-linux-power-button how to
disable Power Button that is used as F20 on PazzPort remote. Note that before remote sends
KEY_F20 also KEY_POWER is sent before. That's why F20 cancels scheduled shutdown

* Dependencies

Debian:

    apt-get install python3-venv
    apt-get install python-evdev
    
* Patching core.py 

NET commands do not all return the same reponse but rather list or time ob playing.
For this reason the following routine is updated (manually):

```
def filter_for_message(getter_func, msg):
    """Helper that calls ``getter_func`` until a matching message
    is found, or the timeout occurs. Matching means the same commands
    group, i.e. for sent message MVLUP we would accept MVL13
    in response."""
    start = time.time()
    network_response_received = False
    while True:
        candidate = getter_func(0.05)
        # It seems ISCP commands are always three characters.
        print('onkyo ' + repr(candidate))
        if candidate and candidate[:3] == msg[:3]:
            return candidate
        if candidate and msg[0] == 'N':
            network_response_received = True
            if candidate[:2] == 'NL': # Network list
                continue
            if candidate[:2] == 'NT': # Network time
                pass
        if network_response_received == True:
            return candidate
        # The protocol docs claim that a response  should arrive
        # within *50ms or the communication has failed*. In my tests,
        # however, the interval needed to be at least 200ms before
        # I managed to see any response, and only after 300ms
        # reproducably, so use a generous timeout.
        if time.time() - start > 5.0:
            raise ValueError('Timeout waiting for response.')
```

or you may apply the patch with

     patch -p3 -d pyenv/lib/python3.9 < eiscp-core.patch    

* How to run tests

See https://python-evdev.readthedocs.io/en/latest/tutorial.html


```
>>> from evdev import InputDevice, categorize, ecodes
>>> dev = evdev.InputDevice('/dev/input/event14')
>>> dev = InputDevice('/dev/input/event14')
>>> for event in dev.read_loop():
  ...     if event.type == ecodes.EV_KEY:
  ...         print(categorize(event))
  ...
  key event at 1572726008.399822, 125 (KEY_LEFTMETA), down
  key event at 1572726008.399822, 19 (KEY_R), down
  key event at 1572726008.407806, 125 (KEY_LEFTMETA), up
  key event at 1572726008.407806, 19 (KEY_R), up
  key event at 1572726008.727587, 18 (KEY_E), down
  key event at 1572726008.735811, 18 (KEY_E), up
  key event at 1572726008.743589, 46 (KEY_C), down
  key event at 1572726008.751553, 46 (KEY_C), up
  key event at 1572726008.759819, 24 (KEY_O), down
  key event at 1572726008.767811, 24 (KEY_O), up
  key event at 1572726008.775773, 48 (KEY_B), down
  key event at 1572726008.783815, 48 (KEY_B), up
  key event at 1572726008.791801, 22 (KEY_U), down
  ...
  
```

* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
