import asyncio, evdev

# OEM Remote KP-810-77 from https://www.ipazzport.com/oem/product-preview/#handlekeyboard
# python -m evdev.evtest

button = evdev.InputDevice('/dev/input/by-id/usb-3412_7856-event-kbd') #0 ECO button USB
mouse = evdev.InputDevice('/dev/input/by-id/usb-ELMCU_iPazzPort-event-kbd') #2 UP, DOWN, LEFT, RIGHT, OK, CONTEXT, F20
keybd = evdev.InputDevice('/dev/input/event5') # VOLUME, HOME, RETURN
system_control = evdev.InputDevice('/dev/input/by-id/usb-ELMCU_iPazzPort-event-if01') # KEY_POWER
power_button = evdev.InputDevice('/dev/input/event1') # KEY_POWER

print(evdev.util.list_devices())
print(system_control.capabilities(verbose=True))
print(keybd.name)

#power_button.grab()


        
async def print_events(device):
    async for event in device.async_read_loop():
        print(device.path, evdev.categorize(event), sep=': ')


        
#for device in button, mouse, keybd, system_control:
#    asyncio.ensure_future(print_events(device))

for event in evdev.util.list_devices():
    device = evdev.InputDevice(event)
    if device.name in ['Power Button', 'HID 3412:7856', 'ELMCU iPazzPort', 'ELMCU iPazzPort Consumer Control']:
        print("Using ", event, device.name)
        asyncio.ensure_future(print_events(device))
    else:
        print("Ignoring ", event, device.name)
    
loop = asyncio.get_event_loop()
loop.run_forever()
