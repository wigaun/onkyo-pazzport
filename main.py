import asyncio
import evdev
import eiscp
import time
import schedule
import os

# 0 'NLSU0-90.6 | Radio 1 Vrhnika (News)'
# 1 "NLSU1-CENTER 80-a (80's)"                                             
# 2 'NLSU2-98.2 | Radio 94 (Rock Music)'                                      
# 3 'NLSU3-Gardel Radio (Tango Music)'                                        
# 4 'NLSU4-Argentine Tango Radio (World Music)'                               
# 5 'NLSU5-Tango Radio Berlin (Tango Music)'                                  
# 6 'NLSU6-RadioActiva Rosario'                                               
# 7 'NLSU7-Radio De Tango (Tango Music)

def select_tunein_preset(line=0):

    state = receiver.raw('ZPWQSTN') # zone power
    
    if state == 'ZPW00': # if zone power turned off
        print("Turning ON ZONE 2")
        receiver.raw('ZPW01') # turn on zone power
        selected = receiver.raw('SLZ2B') # Zone 2 selector NET
        print("Position: ", selected)
    
    receiver.raw('SLZ2B') # Zone 2 selector NET

    receiver.send('NTCTOP') # Go to top menu
    time.sleep(1)
    #receiver.send('NLSL0') # Select Line 0 (TuneIn) 
    #sleep(5)

    # Query NET List Title such as 'NLTF3000002000D0000FFFF00NET'
    list_title = receiver.raw('NLTQSTN')
    if list_title is None or list_title[:3] != 'NLT': 
        return # rarely can NTM... be received 
    cursor_position = int(list_title[7:11], 16)
    print ("Cursor", cursor_position)
    # Move cursor up if needed
    for i in range(cursor_position):
        receiver.raw('NTZUP')

    #list_title = receiver.raw('NLTQSTN') 
    #cursor_position = int(list_title[7:11], 16)
    #print ("Cursor NOW", cursor_position)

        
    receiver.raw('NTZSELECT') # Select TuneIn
    time.sleep(5) # Connecting .... wait

    receiver.raw('NTZSELECT') # Select My Presets
    #list_title = receiver.raw('NLTQSTN')
    
    for i in range(line):
        receiver.raw('NTZDOWN')
        time.sleep(0.1)

    print ("Selecting TuneIn preset #", line)
    receiver.raw('NTZSELECT')
    receiver.raw('ZMT00') # Unmute    
    return

    # The following works only of MAIN is switched on!
    # Not used at all
    state = receiver.raw('NMSQSTN') # zone power
    receiver.send('NLSL0') # Select Line 0 (TuneIn) 
    time.sleep(6) # Conecting ...
    state = receiver.raw('NLTQSTN')
    receiver.send('NLSL0') # Select Line 0 (My Presets)
    time.sleep(6)
    receiver.send(f'NLSL{line}') # Select Line (Selected radio)

    

async def process_events(device, receiver):
    async for event in device.async_read_loop():
        #print(f'{event.code} : {event.value}')

        if event.value == 0x1:      # Key down
            try:  
                if event.code == evdev.ecodes.ecodes['KEY_F20']: 
                    print("POWER on remote")  # Remote power button sends KEY_POWER before KEY_F20
                    os.system("shutdown -c")  # That's why we need to Cancel pending shutdown                  
                    state = receiver.raw('ZPWQSTN') # zone power
                    if state == 'ZPW00': # if zone power turned off
                        select_tunein_preset(0)                        
                    else:
                        receiver.raw('ZPW00') # turn off zone power
                elif event.code == evdev.ecodes.ecodes['KEY_LEFTMETA'] : # Ecogreen button
                    print("POWER on remote or Ecogreen button")
                    state = receiver.raw('ZPWQSTN') # zone power
                    if state == 'ZPW00': # if zone power turned off
                        select_tunein_preset(0)                        
                    else:
                        receiver.raw('ZPW00') # turn off zone power
                elif event.code == evdev.ecodes.ecodes['KEY_POWER']:
                    print("Power button")
                    os.system("shutdown --poweroff +1") # Schedule shutdown

                elif event.code == evdev.ecodes.ecodes['KEY_ENTER']:
                    print("ENTER")
                    select_tunein_preset(4)
                    
                elif event.code == evdev.ecodes.ecodes['KEY_COMPOSE']:
                    print("COMPOSE")
                    select_tunein_preset(5)

                elif event.code == evdev.ecodes.ecodes['KEY_LEFT']:
                    print("LEFT")
                    select_tunein_preset(1)

                elif event.code == evdev.ecodes.ecodes['KEY_RIGHT']:
                    print("RIGHT")
                    select_tunein_preset(6)
                    
                elif event.code == evdev.ecodes.ecodes['KEY_HOMEPAGE']:
                    print("HOME")
                    select_tunein_preset(2)

                elif event.code == evdev.ecodes.ecodes['KEY_UP']:
                    print("UP")
                    receiver.raw("NTCRETURN")
                    receiver.raw("NTCUP")
                    receiver.raw("NTCSELECT")
                                        
                elif event.code == evdev.ecodes.ecodes['KEY_DOWN']:
                    print("UP")
                    receiver.raw("NTCRETURN")
                    receiver.raw("NTCDOWN")
                    receiver.raw("NTCSELECT")

                elif event.code == evdev.ecodes.ecodes['KEY_BACK']: #key back
                    receiver.raw('ZMTTG') # zone mute toggle

                elif event.code == evdev.ecodes.ecodes['KEY_VOLUMEDOWN']: #key volume down
                    receiver.raw('ZVLDOWN') # zone decrease volume

                elif event.code == evdev.ecodes.ecodes['KEY_VOLUMEUP']: #key volume up
                    receiver.raw('ZVLUP') # zone increase volume
            except ValueError as e:
                print("Exception ValueError:", e)



def power_off():
    receiver.raw('ZPW00') # turn off zone power

# https://stackoverflow.com/questions/37512182/how-can-i-periodically-execute-a-function-with-asyncio    
async def periodic(schedule):
    while True:
        schedule.run_pending()
        await asyncio.sleep(20)

def crontab():
    morning = "06:30"
    schedule.every().monday.at(morning).do(select_tunein_preset)
    schedule.every().tuesday.at(morning).do(select_tunein_preset)
    schedule.every().wednesday.at(morning).do(select_tunein_preset)
    schedule.every().thursday.at(morning).do(select_tunein_preset)
    schedule.every().friday.at(morning).do(select_tunein_preset)
    schedule.every().day.at("07:05").do(power_off)
    schedule.every().saturday.at('08:30').do(select_tunein_preset, 5)
    schedule.every().sunday.at('09:30').do(select_tunein_preset, 6)
    asyncio.ensure_future(periodic(schedule))

if __name__ == "__main__":
    receiver = None
    for rec in eiscp.eISCP.discover(timeout=5):
        if rec.info['identifier'] == '0009B0EE055E':
            receiver = rec
            break

    if receiver:
        crontab()
        #dev = [1, 5, 0]
        #for i in dev:
        #    asyncio.ensure_future(process_events(
        #        evdev.InputDevice(f'/dev/input/event{i}'), receiver))
        for event in evdev.util.list_devices():
            device = evdev.InputDevice(event)
            if device.name in ['Power Button',
                               'HID 3412:7856',
                               'ELMCU iPazzPort',
                               'ELMCU iPazzPort Consumer Control']:
                print("Using ", event, device.name)
                asyncio.ensure_future(process_events(device, receiver))
            else:
                print("Ignoring ", event, device.name)
        print(receiver.info)
            
        loop = asyncio.get_event_loop()
        loop.run_forever()
